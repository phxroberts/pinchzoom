﻿using System;
using Android.Content;
using Android.Content.Res;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace PinchZoom
{
	//settingsapp.droid.ListViewLayout
	public class ListViewLayout : ListView
	{
		private static int INVALID_POINTER_ID = -1;
		private int mActivePointerId = INVALID_POINTER_ID;
		private ScaleGestureDetector mScaleDetector;

		private float mLastTouchX;
		private float mLastTouchY;
		private float mPosX;
		private float mPosY;

		public ListViewLayout(Context context) : base(context) 
		{
			mScaleDetector = new ScaleGestureDetector(this.Context,new ScaleListener(this));
		}
		public ListViewLayout(Context context, IAttributeSet attrs) : base(context, attrs) { 
			mScaleDetector = new ScaleGestureDetector(this.Context, new ScaleListener(this));
		}
		public ListViewLayout(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) { 
			mScaleDetector = new ScaleGestureDetector(this.Context, new ScaleListener(this));
		}
		bool m_EnableZoom;
		public bool EnableZoom
		{
			get
			{
				return m_EnableZoom;
			}
			set
			{
				m_EnableZoom = value;
			}
		}
		float mScaleFactor = 1.0f;
		float ScaleFactor
		{
			get
			{
				return mScaleFactor;
			}
			set
			{
				mScaleFactor = value;
			}
		}

		float maxWidth = 0.0f;
		public float MaxWidth
		{
			get
			{
				return maxWidth;
			}
			set
			{
				maxWidth = value;
			}
		}
		float maxHeight = 0.0f;
		public float MaxHeight
		{
			get
			{
				return maxHeight;
			}
			set
			{
				maxHeight = value;
			}
		}

		float viewWidth;
		public float ViewWidth
		{
			get
			{
				return viewWidth;
			}
			set
			{
				viewWidth = value;
			}
		}

		float viewHeight;
		public float ViewHeight
		{
			get
			{
				return viewHeight;
			}
			set
			{
				viewHeight = value;
			}
		}

		protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
		{
			viewWidth = MeasureSpec.GetSize(widthMeasureSpec);
			viewHeight = MeasureSpec.GetSize(heightMeasureSpec);
			base.OnMeasure(widthMeasureSpec, heightMeasureSpec);
		}

		public override bool OnTouchEvent(MotionEvent ev)
		{
			base.OnTouchEvent(ev);

			MotionEventActions action = ev.Action;
			mScaleDetector.OnTouchEvent(ev);
			switch (action & MotionEventActions.Mask) {
				case MotionEventActions.Down:
				{
					float x = ev.GetX();
					float y = ev.GetY();
					mLastTouchX = x;
					mLastTouchY = y;
					mActivePointerId = ev.GetPointerId(0);
					break;
				}
				case MotionEventActions.Move:
				{
					int pointerIndex = ev.FindPointerIndex(mActivePointerId);
					if (pointerIndex > -1)
					{
						float x = ev.GetX(pointerIndex);
						float y = ev.GetY(pointerIndex);
						float dx = x - mLastTouchX;
						float dy = y - mLastTouchY;
						mPosX += dx;
						mPosY += dy;

						if (mPosX > 0.0f)
							mPosX = 0.0f;
						else if (mPosX < maxWidth)
							mPosX = maxWidth;

						if (mPosY > 0.0f)
							mPosY = 0.0f;
						else if (mPosY < maxHeight)
							mPosY = maxHeight;

						mLastTouchX = x;
						mLastTouchY = y;
						this.Invalidate();
					}
					break;
				}
				case MotionEventActions.Up:
				{
					mActivePointerId = INVALID_POINTER_ID;
					break;
				}
				case MotionEventActions.Cancel:
				{
					mActivePointerId = INVALID_POINTER_ID;
					break;
				}
				case MotionEventActions.PointerUp:
				{
					int pointerIndex = ((int)action & (int)MotionEventActions.PointerIndexMask) >> (int)MotionEventActions.PointerIndexShift;
					int pointerId = ev.GetPointerId(pointerIndex);
					if (pointerId == mActivePointerId)
					{
						int newPointerIndex = pointerIndex == 0 ? 1 : 0;
						mLastTouchX = ev.GetX(newPointerIndex);
						mLastTouchY = ev.GetY(newPointerIndex);
						mActivePointerId = ev.GetPointerId(newPointerIndex);
					}					
					break;
				}
				default:
					break;
			}

			return true;
		}

		protected override void OnDraw(Android.Graphics.Canvas canvas)
		{
			base.OnDraw(canvas);

			canvas.Save(Android.Graphics.SaveFlags.Matrix);
			canvas.Translate(mPosX, mPosY);
			canvas.Scale(mScaleFactor, mScaleFactor);
			canvas.Restore();
		}

		protected override void DispatchDraw(Android.Graphics.Canvas canvas)
		{
			canvas.Save(Android.Graphics.SaveFlags.Matrix);
			if (mScaleFactor == 1.0f)
			{
				mPosX = 0.0f;
				mPosY = 0.0f;
			}
			canvas.Translate(mPosX, mPosY);
			canvas.Scale(mScaleFactor, mScaleFactor);
			base.DispatchDraw(canvas);
			canvas.Restore();
			Invalidate();
		}

		private class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener
		{
			ListViewLayout m_ListViewLayout;
			public ScaleListener(ListViewLayout layoutInstance) : base()
			{
				m_ListViewLayout = layoutInstance;
			}
			public override bool OnScale(ScaleGestureDetector detector)
			{
				m_ListViewLayout.ScaleFactor *= detector.ScaleFactor;
				m_ListViewLayout.ScaleFactor = Math.Max(1.0f, Math.Min(m_ListViewLayout.ScaleFactor, 4.0f));
				m_ListViewLayout.MaxWidth = m_ListViewLayout.ViewWidth - (m_ListViewLayout.ViewWidth * m_ListViewLayout.ScaleFactor);
				m_ListViewLayout.MaxHeight = m_ListViewLayout.ViewHeight - (m_ListViewLayout.ViewHeight * m_ListViewLayout.ScaleFactor);
				m_ListViewLayout.Invalidate();
				return true;
				//return base.OnScale(detector);
			}
		}
	}
}
