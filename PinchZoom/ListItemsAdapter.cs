﻿using Android.App;
using Android.Widget;
using Android.OS;
using System.IO;
using Android.Views;
using Java.Lang;
using System;
using System.Linq;
using System.Collections.Generic;
using Android.Content;
using Android.Support.V7.App;


namespace PinchZoom
{
	class ListItemsAdapter : BaseAdapter<string>
	{
		List<string> m_Data;
		Context m_Context;
		public ListItemsAdapter(Context contextInstance, IEnumerable<string> data)
		{
			m_Context = contextInstance;
			m_Data = new List<string>(data);
		}
		public override string this[int position]
		{
			get
			{
				return m_Data[position];
			}
		}

		public override int Count
		{
			get
			{
				return m_Data.Count;
			}
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View v = convertView;
			ListItemsAdapterViewHolder viewHolder = v == null ? null : v.Tag as ListItemsAdapterViewHolder;
			//1) If no view exists create one
			if (v == null)
			{
				v = LayoutInflater.FromContext(m_Context).Inflate(Resource.Layout.ListItem, parent, false);
				v.Tag = viewHolder = new ListItemsAdapterViewHolder
				{
					Item = v.FindViewById<TextView>(Resource.Id.listItem),
					ItemReversed = v.FindViewById<TextView>(Resource.Id.listItemReversed),
					ClickMe = v.FindViewById<Button>(Resource.Id.clickme)
				};
			}
			//2) Populate the view
			viewHolder.ItemReversed.Text = new string(m_Data[position].ToCharArray().Reverse().ToArray());
			viewHolder.Item.Text = m_Data[position];
			viewHolder.ClickMe.Click -= viewHolder.ClickMeEventHandler;
			viewHolder.ClickMe.Click += viewHolder.ClickMeEventHandler;
			return v;
		}
	}


	public class ListItemsAdapterViewHolder : Java.Lang.Object
	{
		public TextView Item { get; set; }
		public TextView ItemReversed { get; set; }
		public Button ClickMe { get; set; }
		public void ClickMeEventHandler(object sender, EventArgs e)
		{
			Toast.MakeText(Application.Context, Item.Text, ToastLength.Short).Show();
		}
	}

}
