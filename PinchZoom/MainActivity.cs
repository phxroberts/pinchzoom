﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace PinchZoom
{
	[Activity(Label = "PinchZoom", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);

			ListView listViewInstance = FindViewById<ListView>(Resource.Id.MainListView);
			if (listViewInstance != null)
			{
				string[] listData = new string[] {
					"First","Second","Third","Fourth","Fifth","Sixth","Seventh","Eight","Ninth","Tenth"
				};
				//ArrayAdapter<string> listAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1,listData);
				ListItemsAdapter listAdapter = new ListItemsAdapter(this, listData);
				listViewInstance.Adapter = listAdapter;
			}
		}
	}
}

